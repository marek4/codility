package main

import "fmt"

func main() {
	A := []int{-1, 3, -4, 5, 1, -6, 2, 1}
	fmt.Println(Solution(A))
}

func Solution(A []int) int {
	sums := []int{0}
	rsums := []int{0}

	for i, sum := 0, 0; i < len(A); i++ {
		sum += A[i]
		sums = append(sums, sum)
	}
	sums = append(sums, 0)

	for i, rsum := len(A)-1, 0; i >= 0; i-- {
		rsum += A[i]
		rsums = append(rsums, rsum)
	}
	rsums = append(rsums, 0)
	for i, j := 0, len(rsums)-1; i < j; i, j = i+1, j-1 {
		rsums[i], rsums[j] = rsums[j], rsums[i]
	}

	for i := 1; i < len(A)+1; i++ {
		if sums[i-1] == rsums[i+1] {
			return i - 1
		}
	}

	return -1
}
