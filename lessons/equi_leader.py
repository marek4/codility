# https://app.codility.com/programmers/lessons/8-leader/equi_leader/

def solution(A):
    leader_stack = []

    for a in A:
        if len(leader_stack) == 0:
            leader_stack.append(a)
        else:
            if a != leader_stack[-1]:
                leader_stack.pop()
            else:
                leader_stack.append(a)

    if len(leader_stack) == 0:
        return 0

    candidate = leader_stack[-1]
    candidate_count = 0
    for a in A:
        if candidate == a:
            candidate_count = candidate_count + 1

    candidate_count_so_far = 0
    equi_leader_count = 0
    for i in range(0, len(A) - 1):
        if candidate == A[i]:
            candidate_count_so_far = candidate_count_so_far + 1
        size_so_far = i + 1
        size_to_end = len(A) - size_so_far
        if candidate_count_so_far > size_so_far // 2 and candidate_count - candidate_count_so_far > size_to_end // 2:
            equi_leader_count = equi_leader_count + 1

    return equi_leader_count

if __name__ == "__main__":
    print(solution([4, 3, 4, 4, 4, 2]))