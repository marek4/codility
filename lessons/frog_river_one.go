package main

import "fmt"

func main() {
	A := []int{1, 3, 1, 4, 2, 3, 5, 6}
	X := 5
	fmt.Println(Solution(X, A))
}

// Solution https://codility.com/programmers/lessons/4-counting_elements/frog_river_one/
func Solution(X int, A []int) int {
	seen := make([]bool, X)
	count := 0
	for key, val := range A {
		if !seen[val-1] {
			seen[val-1] = true
			if count++; count == X {
				return key
			}
		}
	}
	return -1
}
