package main

import "fmt"

func main() {
	S := "(()(())())"
	fmt.Println(Solution(S))
}

// Solution https://codility.com/programmers/lessons/7-stacks_and_queues/nesting/
func Solution(S string) int {
	if len(S) == 0 {
		return 1
	}
	if len(S)%2 == 1 {
		return 0
	}

	counter := 0
	for _, c := range S {
		if c == '(' {
			counter++
		} else {
			counter--
			if counter < 0 {
				return 0
			}
		}
	}
	if counter != 0 {
		return 0
	}

	return 1
}
