# https://app.codility.com/programmers/lessons/9-maximum_slice_problem/max_slice_sum/

def solution(A):
    max_ending_here = max_so_far = A[0]
    for x in A[1:]:
        max_ending_here = max(x, max_ending_here + x)
        max_so_far = max(max_so_far, max_ending_here)
    return max_so_far

if __name__ == "__main__":
    print(solution([3 ,2 ,-6,4 ,0]))