package main

import (
	"fmt"
	"sort"
)

func main() {
	A := []int{-3, 1, 2, -2, 5, 6}
	fmt.Println(Solution(A))
}

// Solution https://codility.com/programmers/lessons/6-sorting/max_product_of_three/
func Solution(A []int) int {
	sort.Sort(sort.IntSlice(A))
	maxPositive := A[len(A)-3] * A[len(A)-2] * A[len(A)-1]
	maxNegative := A[0] * A[1] * A[len(A)-1]
	if maxPositive > maxNegative {
		return maxPositive
	}
	return maxNegative
}
