# https://app.codility.com/programmers/lessons/8-leader/dominator/

def solution(A):
    leader_stack = []

    for a in A:
        if len(leader_stack) == 0:
            leader_stack.append(a)
        else:
            if a != leader_stack[-1]:
                leader_stack.pop()
            else:
                leader_stack.append(a)

    if len(leader_stack) == 0:
        return -1

    candidate = leader_stack[-1]
    candidate_count = 0
    candidate_index = -1
    for idx, a in enumerate(A):
        if candidate == a:
            candidate_index = idx
            candidate_count = candidate_count + 1

    if candidate_count > len(A) // 2:
        return candidate_index
    return -1

if __name__ == "__main__":
    print(solution([3, 4, 3, 2, 3, -1, 3, 3]))