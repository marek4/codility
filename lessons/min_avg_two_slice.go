package main

import "fmt"

func main() {
	A := []int{4, 2, 2, 5, 1, 5, 8}
	fmt.Println(Solution(A))
}

// Solution https://codility.com/programmers/lessons/5-prefix_sums/min_avg_two_slice/
func Solution(A []int) int {
	pairsMinIndex := 0
	pairsMinAvg := float64(A[pairsMinIndex]+A[pairsMinIndex+1]) / 2

	for i := 1; i < len(A)-1; i++ {
		newAvg := float64(A[i]+A[i+1]) / 2
		if newAvg < pairsMinAvg {
			pairsMinIndex = i
			pairsMinAvg = newAvg
		}
	}

	//check if going backwards could give us better solution
	currentLen := 2
	currentSum := pairsMinAvg * float64(currentLen)
	currentAvg := pairsMinAvg
	minIndex := pairsMinIndex

	if len(A) > 2 {
		triplesMinIndex := 0
		triplesMinAvg := float64(A[triplesMinIndex]+A[triplesMinIndex+1]+A[triplesMinIndex+2]) / 3

		for i := 1; i < len(A)-2; i++ {
			newAvg := float64(A[i]+A[i+1]+A[i+2]) / 3
			if newAvg < triplesMinAvg {
				triplesMinIndex = i
				triplesMinAvg = newAvg
			}
		}
		if triplesMinAvg < pairsMinAvg {
			currentLen = 3
			currentSum = triplesMinAvg * float64(currentLen)
			currentAvg = triplesMinAvg
			minIndex = triplesMinIndex
		}
	}

	newLen := currentLen
	newSum := currentSum
	newAvg := currentAvg

	for i := minIndex - 1; i >= 0; i-- {
		newLen++
		newSum += float64(A[i])
		newAvg = float64(newSum) / float64(newLen)
		if newAvg < currentAvg {
			currentLen = newLen
			currentSum = newSum
			currentAvg = newAvg
			minIndex = i
		}
	}

	return minIndex
}
