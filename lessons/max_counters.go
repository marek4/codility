package main

import "fmt"

func main() {
	A := []int{3, 4, 4, 6, 1, 4, 4}
	N := 5
	fmt.Println(Solution(N, A))
}

// Solution https://codility.com/programmers/lessons/4-counting_elements/max_counters/
func Solution(N int, A []int) []int {
	counters := make([]int, N)
	lastMax := 0
	max := 0
	for _, val := range A {
		if val <= N {
			if counters[val-1] < lastMax {
				counters[val-1] = lastMax
			}
			counters[val-1]++
			if counters[val-1] > max {
				max = counters[val-1]
			}
		} else {
			lastMax = max
		}
	}
	for key, val := range counters {
		if val < lastMax {
			counters[key] = lastMax
		}
	}
	return counters
}
