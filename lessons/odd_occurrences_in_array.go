package main

import "fmt"

func main() {
	A := []int{9, 3, 9, 3, 9, 7, 9}
	fmt.Println(Solution(A))
}

// Solution https://codility.com/programmers/lessons/2-arrays/odd_occurrences_in_array/
func Solution(A []int) int {
	result := A[0]
	for i := 1; i < len(A); i++ {
		result ^= A[i]
	}
	return result
}
