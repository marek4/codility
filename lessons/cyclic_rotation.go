package main

import "fmt"

func main() {
	A := []int{3, 8, 9, 7, 6}
	K := 3
	fmt.Println(Solution(A, K))
}

// Solution https://codility.com/programmers/lessons/2-arrays/cyclic_rotation/
func Solution(A []int, K int) []int {
	result := make([]int, len(A))
	for i := 0; i < len(A); i++ {
		result[(i+K)%len(A)] = A[i]
	}
	return result
}
