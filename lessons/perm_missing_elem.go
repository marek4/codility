package main

import "fmt"

func main() {
	A := []int{2, 3, 1, 5}
	fmt.Println(Solution(A))
}

// Solution https://codility.com/programmers/lessons/3-time_complexity/perm_missing_elem/
func Solution(A []int) int {
	B := make([]bool, len(A)+2)
	for _, val := range A {
		B[val] = true
	}
	for i := 1; i < len(B); i++ {
		if B[i] == false {
			return i
		}
	}
	return 0
}
