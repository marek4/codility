# https://app.codility.com/programmers/lessons/10-prime_and_composite_numbers/min_perimeter_rectangle/

import math

def solution(N):
    s = math.floor(math.sqrt(N))

    for a in range(s, 0, -1):
        if N % a == 0:
            return 2 * (a + (N // a))

if __name__ == "__main__":
    print(solution(30))