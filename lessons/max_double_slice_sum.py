# https://app.codility.com/programmers/lessons/9-maximum_slice_problem/max_double_slice_sum/

def solution(A):
    max_ending_left = 0
    max_endings_left = [max_ending_left]
    for x in A[1:]:
        max_ending_left = max(0, max_ending_left + x)
        max_endings_left.append(max_ending_left)

    max_ending_right = 0
    max_endings_right = [max_ending_right]
    for x in reversed(A[0:-1]):
        max_ending_right = max(0, max_ending_right + x)
        max_endings_right.append(max_ending_right)
    max_endings_right = list(reversed(max_endings_right))

    current_max = 0
    for i in range(1, len(A) - 1):
        test_max = max_endings_left[i - 1] + max_endings_right[i + 1]
        if test_max > current_max:
            current_max = test_max

    return current_max


if __name__ == "__main__":
    print(solution([3, 2, 6, -1, 4, 5, -1, 2]))