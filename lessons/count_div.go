package main

import "fmt"

func main() {
	A := 6
	B := 11
	K := 2
	fmt.Println(Solution(A, B, K))
}

// Solution https://codility.com/programmers/lessons/5-prefix_sums/count_div/
func Solution(A int, B int, K int) int {
	N := B/K - A/K
	if A%K == 0 {
		N++
	}
	return N
}
