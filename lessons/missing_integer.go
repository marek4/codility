package main

import "fmt"

func main() {
	A := []int{1, 3, 6, 4, 1, 2}
	fmt.Println(Solution(A))
}

// Solution https://codility.com/programmers/lessons/4-counting_elements/missing_integer/
func Solution(A []int) int {
	found := make(map[int]bool)
	max := A[0]
	for _, val := range A {
		found[val] = true
		if max < val {
			max = val
		}
	}
	for i := 1; i <= max+1; i++ {
		if _, ok := found[i]; !ok {
			return i
		}
	}
	return 1
}
