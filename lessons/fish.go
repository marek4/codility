package main

import (
	"fmt"
)

func main() {
	A := []int{4, 3, 2, 1, 5}
	B := []int{0, 1, 0, 0, 0}
	fmt.Println(Solution(A, B))
}

// Solution https://codility.com/programmers/lessons/7-stacks_and_queues/fish/
func Solution(A []int, B []int) int {
	stack := make([]int, len(A))
	pointer := 0

	eaten := 0
	for i := 0; i < len(A); i++ {
		if B[i] == 1 {
			stack[pointer] = A[i]
			pointer++
		}
		if B[i] == 0 && pointer > 0 {
			if stack[pointer-1] > A[i] {
				eaten++
			} else {
				for pointer > 0 && stack[pointer-1] < A[i] {
					pointer--
					eaten++
				}
				i--
			}
		}
	}
	return len(A) - eaten
}
