package main

import "fmt"
import "math"

func main() {
	A := []int{3, 1, 2, 4, 3}
	fmt.Println(Solution(A))
}

// Solution https://codility.com/programmers/lessons/3-time_complexity/tape_equilibrium/
func Solution(A []int) int {
	sums := []int{}
	rsums := []int{}

	for i, sum := 0, 0; i < len(A); i++ {
		sum += A[i]
		sums = append(sums, sum)
	}

	for i, rsum := len(A)-1, 0; i >= 0; i-- {
		rsum += A[i]
		rsums = append(rsums, rsum)
	}
	for i, j := 0, len(rsums)-1; i < j; i, j = i+1, j-1 {
		rsums[i], rsums[j] = rsums[j], rsums[i]
	}

	min := math.MaxFloat64
	for i := 0; i < len(A)-1; i++ {
		diff := math.Abs(float64(sums[i]) - float64(rsums[i+1]))
		if diff < min {
			min = diff
		}
	}

	return int(min)
}
