package main

import "fmt"

func main() {
	A := []int{2, 1, 1, 2, 3, 1}
	fmt.Println(Solution(A))
}

// Solution https://codility.com/programmers/lessons/6-sorting/distinct/
func Solution(A []int) int {
	result := make(map[int]bool)
	for _, val := range A {
		result[val] = true
	}
	return len(result)
}
