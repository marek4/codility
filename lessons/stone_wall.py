# https://app.codility.com/programmers/lessons/7-stacks_and_queues/stone_wall/

def solution(H):
    height_stack = []
    current_height = 0
    squares_count = 0

    for h in H:
        while h < current_height and len(height_stack) > 0:
            height_stack.pop()
            squares_count = squares_count + 1
            if len(height_stack) > 0:
                current_height = height_stack[-1]
            else:
                current_height = 0
        if h > current_height:
            height_stack.append(h)
            current_height = h

    squares_count = squares_count + len(height_stack)
    return squares_count

if __name__ == "__main__":
    print(solution([8, 8, 5, 7, 9, 8, 7, 4, 8]))