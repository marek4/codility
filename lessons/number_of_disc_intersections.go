package main

import (
	"fmt"
	"sort"
)

func main() {
	A := []int{1, 5, 2, 1, 4, 0}
	fmt.Println(Solution(A))
}

// Solution https://codility.com/programmers/lessons/6-sorting/number_of_disc_intersections/
func Solution(A []int) int {
	intersections := 0
	left := make([]int, len(A))
	right := make([]int, len(A))

	for i := 0; i < len(A); i++ {
		left[i] = i - A[i]
		right[i] = i + A[i]
	}
	sort.Sort(sort.IntSlice(left))
	sort.Sort(sort.IntSlice(right))

	for i, j := 0, 0; i < len(A); i++ {
		for j < len(A) && left[j] <= right[i] {
			intersections += j - i
			j++
		}
		if intersections > 10000000 {
			return -1
		}
	}

	return intersections
}
