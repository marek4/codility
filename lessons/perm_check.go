package main

import "fmt"

func main() {
	A := []int{4, 1, 3, 2}
	fmt.Println(Solution(A))
}

// Solution https://codility.com/programmers/lessons/4-counting_elements/perm_check/
func Solution(A []int) int {
	max := A[0]
	sum := 0
	for _, val := range A {
		sum += val
		if val > max {
			max = val
		}
	}
	if float64(sum) != (float64(1+max)/2.)*float64(max) {
		return 0
	}
	B := make([]bool, max)
	for _, val := range A {
		if B[val-1] {
			return 0
		}
		B[val-1] = true
	}
	for _, val := range B {
		if val == false {
			return 0
		}
	}
	return 1
}
