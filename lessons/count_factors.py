# https://app.codility.com/programmers/lessons/10-prime_and_composite_numbers/count_factors/

import math

def solution(N):
    s = math.floor(math.sqrt(N))
    count_factors = 0

    for a in range(1, s + 1):
        if N % a == 0:
            count_factors += 2

    if math.sqrt(N) == s:
        count_factors -= 1

    return count_factors

if __name__ == "__main__":
    print(solution(24))