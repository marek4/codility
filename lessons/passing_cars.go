package main

import "fmt"

func main() {
	A := []int{0, 1, 0, 1, 1}
	fmt.Println(Solution(A))
}

// Solution https://codility.com/programmers/lessons/5-prefix_sums/passing_cars/
func Solution(A []int) int {
	passingCars := 0
	east := 0
	for _, val := range A {
		if val == 0 {
			east++
		} else {
			passingCars += east
			if passingCars > 1000000000 {
				return -1
			}
		}
	}
	return passingCars
}
