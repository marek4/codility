package main

import "fmt"

func main() {
	S := "{[()()]}"
	fmt.Println(Solution(S))
}

// Solution https://codility.com/programmers/lessons/7-stacks_and_queues/brackets/
func Solution(S string) int {
	if len(S) == 0 {
		return 1
	}
	if len(S)%2 == 1 {
		return 0
	}

	stack := make([]rune, len(S))
	pointer := 0

	for _, c := range S {
		if c == '{' || c == '[' || c == '(' {
			stack[pointer] = c
			pointer++
		} else {
			if pointer == 0 {
				return 0
			} else {
				pointer--
				s := stack[pointer]
				if (c == '}' && s == '{') || (c == ']' && s == '[') || (c == ')' && s == '(') {
					continue
				}
				return 0
			}
		}
	}
	if pointer != 0 {
		return 0
	}

	return 1
}
