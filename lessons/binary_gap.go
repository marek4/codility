package main

import "fmt"

func main() {
	N := 1041
	fmt.Println(Solution(N))
}

// Solution https://codility.com/programmers/lessons/1-iterations/binary_gap/
func Solution(N int) int {
	longest := 0
	current := 0
	foundOne := false
	for {
		if N == 0 {
			break
		}
		if N%2 == 1 {
			current = 0
			foundOne = true
		} else {
			if foundOne {
				current++
				if current > longest {
					longest = current
				}
			}
		}
		N = N >> 1
	}
	return longest
}
