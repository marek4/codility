package main

import "fmt"

func main() {
	S := "CAGCCTA"
	P := []int{2, 5, 0}
	Q := []int{4, 5, 6}
	fmt.Println(Solution(S, P, Q))
}

// Solution https://codility.com/programmers/lessons/5-prefix_sums/genomic_range_query/
func Solution(S string, P []int, Q []int) []int {
	//swap letters to impact factors
	impactFactors := make([]int, len(S))
	for key, val := range S {
		impactFactor := 0
		switch val {
		case 'A':
			impactFactor = 1
		case 'C':
			impactFactor = 2
		case 'G':
			impactFactor = 3
		case 'T':
			impactFactor = 4
		}
		impactFactors[key] = impactFactor
	}
	//count occurents at position for each factor
	impactFactorsCounts := make([][]int, 4)
	for i := 0; i < 4; i++ {
		count := 0
		impactFactorsCounts[i] = make([]int, len(S))
		for key, val := range impactFactors {
			if i+1 == val {
				count++
			}
			impactFactorsCounts[i][key] = count
		}
	}

	K := make([]int, len(P))
	for i := 0; i < len(P); i++ {
		p := P[i]
		q := Q[i]
		k := impactFactors[p]
		for j := 0; j < 4 && j+1 < k; j++ {
			if impactFactorsCounts[j][p] < impactFactorsCounts[j][q] {
				k = j + 1
				break
			}
		}
		K[i] = k
	}
	return K
}
