package main

import (
	"fmt"
	"sort"
)

func main() {
	A := []int{10, 2, 5, 1, 8, 20}
	fmt.Println(Solution(A))
}

// Solution https://codility.com/programmers/lessons/6-sorting/triangle/
func Solution(A []int) int {
	sort.Sort(sort.IntSlice(A))
	for i := 0; i < len(A)-2; i++ {
		if A[i]+A[i+1] > A[i+2] && A[i+2]+A[i] > A[i+1] && A[i+1]+A[i+2] > A[i] {
			return 1
		}
	}
	return 0
}
