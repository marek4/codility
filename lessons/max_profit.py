# https://app.codility.com/programmers/lessons/9-maximum_slice_problem/max_profit/

def solution(A):
    changes = []
    for i in range(0, len(A) - 1):
        changes.append(A[i+1] - A[i])
    return max(0, max_subarray(changes))

def max_subarray(A):
    if not A:
        return 0
    max_ending_here = max_so_far = A[0]
    for x in A[1:]:
        max_ending_here = max(x, max_ending_here + x)
        max_so_far = max(max_so_far, max_ending_here)
    return max_so_far

if __name__ == "__main__":
    print(solution([23171, 21011, 21123, 21366, 21013, 21367]))