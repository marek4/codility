package main

import "fmt"

func main() {
	A := []int{2, 2, 1, 0, 1}
	fmt.Println(Solution(A))
}

// Solution https://codility.com/programmers/task/prefix_set/
func Solution(A []int) int {
	firstOccurence := make(map[int]int)

	for i := 0; i < len(A); i++ {
		if _, ok := firstOccurence[A[i]]; !ok {
			firstOccurence[A[i]] = i
		}
	}

	maxVal := -1
	for _, val := range firstOccurence {
		if val > maxVal {
			maxVal = val
		}
	}
	return maxVal
}
