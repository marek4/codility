package main

import "fmt"

func main() {
	S := "baababa"
	fmt.Println(Solution(S))
}

// Solution https://codility.com/programmers/task/count_palindromic_slices/
func Solution(S string) int {
	pairs := make([]int, 0)
	for i := 0; i < len(S)-1; i++ {
		if S[i] == S[i+1] {
			pairs = append(pairs, i)
		}
	}

	triples := make([]int, 0)
	for i := 0; i < len(S)-2; i++ {
		if S[i] == S[i+2] {
			triples = append(triples, i)
		}
	}

	pairsExtensions := make([]int, 0)
	for i := 0; i < len(pairs); i++ {
		for j := 1; pairs[i]-j >= 0 && pairs[i]+j+1 < len(S) && S[pairs[i]-j] == S[pairs[i]+j+1]; j++ {
			pairsExtensions = append(pairsExtensions, pairs[i]-j)
		}

	}

	triplesExtensions := make([]int, 0)
	for i := 0; i < len(triples); i++ {
		for j := 1; triples[i]-j >= 0 && triples[i]+j+2 < len(S) && S[triples[i]-j] == S[triples[i]+j+2]; j++ {
			triplesExtensions = append(triplesExtensions, triples[i]-j)
		}

	}

	return len(pairs) + len(pairsExtensions) + len(triples) + len(triplesExtensions)
}
