package main

import "fmt"

func main() {
	A := []int{1, 5, 2, 1, 4, 0}
	fmt.Println(Solution(A))
}

// Solution https://codility.com/programmers/task/number_of_disc_intersections/
func Solution(A []int) int {
	intersections := 0

	for i := 1; i < len(A); i++ {
		for j := 0; j < i; j++ {
			if i-A[i] <= j+A[j] {
				intersections++
				if intersections > 10000000 {
					return -1
				}
			}
		}
	}

	return intersections
}
